package com.lzj.vo;

public class Stock {
	private int id;
	private int stock;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	@Override
	public String toString() {
		return "Stock [id=" + id + ", stock=" + stock + "]";
	}
}
