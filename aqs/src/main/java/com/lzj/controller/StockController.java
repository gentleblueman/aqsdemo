package com.lzj.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lzj.service.TradeService;

/**
 * 模拟高并发扣减库存接口
 * @author roseonly
 *
 */
@RestController
public class StockController {
	
	@Autowired
	private TradeService tradeService;

	@RequestMapping("/order")
	public String order() {
		String result = tradeService.decStockNoLock();
		return result;
	}
	
 }
