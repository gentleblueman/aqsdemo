package com.lzj.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.lzj.vo.Stock;

@Mapper
public interface TradeMapper {

	Stock select(int id);
	void update(Stock stock);
}
