package com.lzj.utils;

import java.lang.reflect.Field;

import sun.misc.Unsafe;

public class UnsafeInstance {

	/**
	 * 反射获取Unsafe
	 * @return
	 */
	public static Unsafe reflectGetUnsafe() {
		try {
			Field field = Unsafe.class.getDeclaredField("theUnsafe");
			field.setAccessible(true);
			return (Unsafe) field.get(null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
